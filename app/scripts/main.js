require.config({
    paths: {
        jquery: '../components/jquery/jquery',
        // bootstrap: 'vendor/bootstrap',
        backbone:  '../components/backbone/backbone',
        underscore: '../components/underscore/underscore',
        masonry: '../components/jquery-masonry/jquery.masonry',
    },
    shim: {
      backbone: {
        deps: ['underscore'],
        exports: 'Backbone'
      },
      underscore: {
        exports: '_'
      },
      masonry: ['jquery']
        // bootstrap: {
        //     deps: ['jquery'],
        //     exports: 'jquery'
        // }
    }
});

require([
  'app',
  'jquery',
  'backbone',
  'Router',

  /**
   * the following simply attach to app, making them
   * available to anything that uses app.
   */
  // Models.
  'models/Comment',
  'models/Item',
  'models/Marker',
  'models/User',
  'models/Journal',

  // Collections.
  'collections/Comments',
  'collections/Items',
  'collections/Markers',

  // Views.
  'views/item/stream',
  'views/item/photo',
  'views/item/text',

  'views/comment/list',
  'views/comment/detail',

  'views/Journal',

  'views/user/header',
  'views/user/profile',

  'views/bar_modules/comment',
  'views/bar_modules/photo',
  'views/bar_modules/text',
  'views/bar_modules/settings',
  'views/bar_modules/marker',

  ], function (app, $, Backbone, Router) {
    'use strict';
    // use app here
    app.router = new Router();
    // Trigger the initial route and enable HTML5 History API support, set the
    // root folder to '/' by default.  Change in app.js.
    Backbone.history.start({ pushState: true, root: app.root });

    // All navigation that is relative should be passed through the navigate
    // method, to be processed by the router. If the link has a `data-bypass`
    // attribute, bypass the delegation completely.
    $(document).on('click', 'a:not([data-bypass])', function(evt) {
      // Get the absolute anchor href.
      var href = $(this).attr('href');

      // If the href exists and is a hash route, run it through Backbone.
      if (href && href.indexOf('#') === 0) {
        // Stop the default event to ensure the link will not cause a page
        // refresh.
        evt.preventDefault();

        // `Backbone.history.navigate` is sufficient for all Routers and will
        // trigger the correct events. The Router's internal `navigate` method
        // calls this anyways.  The fragment is sliced from the root.
        Backbone.history.navigate(href, true);
      }
    });
});
