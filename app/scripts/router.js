define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';
  var loadedUser = false;

  var Router = Backbone.Router.extend({
    routes: {
      '': 'index',
      'journal/:id': 'journal',
      'journal': 'journal'
    },

    index: function () {
      loadUser();
    },

    login: function () {

    },

    /**
     * Load the data needed for viewing a journal.
     * data: {
     *   journal: {user:{}, markers:{}, items:{}}
     * }
     */
    journal: function (id) {

      loadUser(function () {

        if (!id && app.user) id = app.user.get('_id'); // Viewing personal journal.

        var journalModel = new app.Model.Journal({_id:id});
        journalModel.fetch({
          success: function (model) {
            var journal = new app.View.Journal({model: model});
            $('body').append(journal.render().el);
            journal.postRender();
          }
        });
      });
    }

  });

  /**
   * Load the main user data in.
   */
  function loadUser (cb) {
    if (!loadedUser) {
      $.get('/api/user/session', function (data) {
        loadedUser = true;

        if (data.user) app.user = new app.Model.User(data.user);

        cb();
      });
    }
  }

  /**
   * Load the journal data in.
   */
  // function loadJournal (id, cb) {
  //   $.get('api/journal/' + id, function (data) {
  //     // data.journal.user = new app.Model.User(data.journal.user);
  //     // data.journal.items = new app.Collection.Items(data.journal.items);
  //     console.log('loaded journal data');
  //     cb(data.journal);
  //   });
  // }

  return Router;

});