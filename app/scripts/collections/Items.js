/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'models/Item'
],

function($, _, Backbone, app) {
  'use strict';

  app.Collection.Items = Backbone.Collection.extend({

    model: app.Model.Item,
    url: 'api/items',

    initialize: function () {
      this.on('add', function(model) {
        console.log('Added item to collection.');
      });

      // this.on("remove", function(model) {
      //   console.log("Removed " + model.get('title'));
      // });

      // this.on("change:completed", function(model) {
      //   console.log("Completed " + model.get('title'));
      // });
    }

  });
});
