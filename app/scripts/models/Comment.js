/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.Model.Comment = Backbone.Model.extend({
    defaults: {

    },

    idAttribute: '_id',

    // url: 'api/comment',

    initialize: function () {

    }
  });
});
