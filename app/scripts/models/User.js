/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.Model.User = Backbone.Model.extend({
    defaults: {
      profile_picture: 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash4/c218.91.546.546/s320x320/206486_10151143730086792_578892979_n.jpg'
    },

    idAttribute: '_id',

    url: '/api/user',

    initialize: function () {

    }
  });
});
