// defaults: {
//     title: '',
//     completed: false
//   }
// this.on('change:title', function(){
//   console.log('Title value for this model has changed.');
// });
// called on save() or set()
  // validate: function(attribs){
  //   if(attribs.title === undefined){
  //       return "Remember to set a title for your todo.";
  //   }
  // },

/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.Model.Item = Backbone.Model.extend({
    defaults: {

    },

    idAttribute: '_id',

    url: '/api/item',

    initialize: function () {

    }
  });
});
