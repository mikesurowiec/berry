/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.Model.Marker = Backbone.Model.extend({
    defaults: {

    },

    idAttribute: '_id',

    url: '/api/marker',

    initialize: function () {

    }
  });
});
