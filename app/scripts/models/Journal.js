/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.Model.Journal = Backbone.Model.extend({
    defaults: {
    },
    idAttribute: '_id',
    urlRoot: '/api/journal',
    initialize: function () {

    }
  });
});
