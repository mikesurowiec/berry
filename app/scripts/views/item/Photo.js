/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/items/photo.ejs'
],

function($, _, Backbone, app, PhotoTemplate) {
  'use strict';

  app.View.Item.Photo = Backbone.View.extend({

    initialize: function () {

    },

    model: app.Model.Item,

    template: _.template(PhotoTemplate),

    //template: photo_item
    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }

  });
});
