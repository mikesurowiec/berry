/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/items/text.ejs'
],

function($, _, Backbone, app, TextTemplate) {
  'use strict';

  app.View.Item.Text = Backbone.View.extend({

    initialize: function () {

    },

    model: app.Model.Text,

    template: _.template(TextTemplate),

    //template: photo_item
    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }

  });
});
