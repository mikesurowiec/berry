/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'masonry',
  'vendor/text!templates/items/stream.ejs'

],

function($, _, Backbone, app, Masonry, StreamTemplate) {
  'use strict';

  app.View.Item.List = Backbone.View.extend({
    tagName: 'div',
    id: 'stream',
    className: '',
    template: _.template(StreamTemplate),
    collection: app.Collection.Items,

    initialize: function () {
      // this.render();
      var self = this;

      app.on('stream.grow', function () {
        self.$el.removeClass('shrink');
        // self.$el.masonry(); //use 'reload' to prepend items
        self.masonize();
      });

      app.on('stream.shrink', function () {
        self.$el.addClass('shrink');
        // self.$el.masonry();
        self.masonize();
      });

      self.listenTo(self.collection, 'add', self.addSingleItem);

      self.colCount = 0;
      self.colWidth = 0;
      self.margin = 10;
      self.windowWidth = 0;
      self.blocks = [];
    },

    events: {

    },

    render: function () {
      var self = this;
      self.collection.each(function(item) {
        self.renderItem.call(self, item);
      });
      return this;
    },

    renderItem: function (item) {
      var self = this;
      var view;
      var type = item.get('type');

      if (type === 'photo') {
        view = new app.View.Item.Photo({ model: item });
      } else if (type === 'text') {
        view = new app.View.Item.Text({ model: item });
      }
      if (!item.get('size')) {
        item.set('size', 'col1');
      }
      self.$el.append(view.render().el);
      return this;
    },

    addSingleItem: function (item) {
      var self = this;
      self.renderItem(item);
      self.masonize('reload');
    },

    fitText: function () {
      var self = this;
      var $items = self.$el.find('.text-item');
      $items.each(function() {
        var $text = $(this).find('p');
        $text.css('font-size', '80px');
        while($text.height() > $(this).height() - 10) {
          var nextSize = (parseInt($text.css('font-size'), 10) - 1);
          $text.css('font-size', nextSize + 'px' );
          if (parseInt($text.css('font-size'), 10) < 15) {
            $(this).css('overflow-y', 'scroll');
            break;
          }
        }
      });
    },

    remove: function () {

    },

    masonize: function (action) {
      var self = this;
      if (action === 'reload') {
        self.$el.masonry('reload');
      } else {
        self.$el.imagesLoaded(function () {
          self.$el.masonry({
            columnWidth: 1,
            gutterWidth: 0,
            itemSelector : '.item'
            // isFitWidth: true,
            // isAnimated: true
          });
        });
      }

      // $(window).resize(self.setupItems.bind(self));
    },

    // setupItems: function () {
    //   var self = this;
    //   self.windowWidth = self.$el.width();
    //   self.colWidth = 282; //$('.item').outerWidth();
    //   self.blocks = [];
    //   self.colCount = Math.floor(self.windowWidth/(self.colWidth+self.margin*2));
    //   for(var i=0;i<self.colCount;i++){
    //     self.blocks.push(self.margin);
    //   }
    //   self.positionItems();
    // },

    // positionItems: function () {
    //   var self = this;
    //   $('.item').each(function(){
    //     console.log(self.blocks);
    //     var min = Math.min.apply(Math, self.blocks);
    //     var index = $.inArray(min, self.blocks);
    //     var leftPos = self.margin+(index*(self.colWidth+self.margin));
    //     $(this).css({
    //       'left':leftPos+'px',
    //       'top':min+'px'
    //     });
    //     console.log('height', $(this).outerHeight());
    //     self.blocks[index] = min+$(this).outerHeight()+self.margin;
    //   });
    // }

  });
});
