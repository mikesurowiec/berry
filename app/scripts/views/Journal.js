define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/Journal.ejs',
  'collections/Items'
],

function($, _, Backbone, app, journalTemplate) {
  'use strict';

  app.View.Journal = Backbone.View.extend({
    tagName: 'div',
    id: 'journal',
    className: 'row',
    model: app.Model.Journal,

    template: _.template(journalTemplate),

    initialize: function () {
      var self = this;
      self.userHeaderView = new app.View.User.Header({model: new app.Model.User(self.model.get('user'))});
      app.items = new app.Collection.Items(self.model.get('items'));
      console.log(app.items);
      self.streamView = new app.View.Item.List({collection: app.items});

      var getStackTrace = function() {
        var obj = {};
        Error.captureStackTrace(obj, getStackTrace);
        return obj.stack;
      };

      // console.log(getStackTrace());

      // self.commentsView = new app.View.Comment.List();
      console.log('initialized journal');
      // app.on('added new item', function (itemData) {
      //   console.log('new item...');
      //   console.log(getStackTrace());

      //   app.items.add(itemData);
      //   // self.streamView.renderItem(itemData);
      //   // self.streamView.masonize('reload');
      // });



    },

    render: function () {
      var self = this;

      self.$el.append(self.userHeaderView.render().el);
      self.$el.append(self.streamView.render().el);

      // self.$el.append(self.commentsView.render().el);

      // self.streamView.fitText();
      setTimeout(function() {
        self.streamView.masonize();
      },0);

      return this;
    },

    postRender: function () {
      var self = this;
      self.userHeaderView.postRender();
    }

  });
});
