/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/user/header.ejs'
],

function($, _, Backbone, app, HeaderTemplate) {
  'use strict';

  app.View.User.Header = Backbone.View.extend({
    model: app.Model.User,
    template: _.template(HeaderTemplate),
    className: 'header-wrap',

    events: {

    },

    initialize: function () {
      var self = this;
      if (self.model) {
        self.settingsModule = new app.View.BarModule.Settings();
        self.markerModule = new app.View.BarModule.Marker();
        self.photoModule = new app.View.BarModule.Photo();
        self.textModule = new app.View.BarModule.Text();
        self.commentModule = new app.View.BarModule.Comment();
      } else {
        self.commentModule = new app.View.BarModule.Comment();
      }
    },

    render: function () {
      var self = this;
      var data = self.model.toJSON();

      data.session_user = app.user.toJSON();
      self.$el.html(this.template(data));
      var $sidebar = self.$el.find('#sidebar');
      if (app.user) {
        $sidebar.append(self.textModule.render().$el);
        $sidebar.append(self.photoModule.render().$el);
        $sidebar.append(self.markerModule.render().$el);
        $sidebar.append(self.settingsModule.render().$el);
        $sidebar.append(self.commentModule.render().$el);
      } else {
        $sidebar.append(self.commentModule.render().$el);
      }

      return this;
    },

    postRender: function () {
      var self = this;
      self.photoModule.postRender();
    },

    remove: function () {

    }

  });
});
