/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app'
],

function($, _, Backbone, app) {
  'use strict';

  app.View.Comment.List = Backbone.View.extend({
    tagName: 'div',
    id: 'comments',

    initialize: function () {

    },

    events: {

    },

    render: function () {
      var self = this;

      // var commentView;
      // this.collection.each(function(comment) {
      // var commentView = new app.Views.Items.Photo({ model: item });


      //   self.$el.append(view.render().el);
      // });

      return this;
    },

    remove: function () {

    }

  });
});
