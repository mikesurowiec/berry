/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/bar_modules/settings-bar.ejs'
],

function($, _, Backbone, app, Template) {
  'use strict';

  app.View.BarModule.Settings = Backbone.View.extend({
    template: _.template(Template),
    className: 'settings-module module',

    events: {
      'click': 'editSettings'
    },

    initialize: function () {
      var self = this;
    },

    render: function () {
      var self = this;

      self.$el.html(this.template());
      return this;
    },

    remove: function () {

    },

    editSettings: function () {
      var self = this;
    },

  });
});
