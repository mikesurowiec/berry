/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/bar_modules/comment-bar.ejs'
],

function($, _, Backbone, app, CommentTemplate) {
  'use strict';

  app.View.BarModule.Comment = Backbone.View.extend({
    template: _.template(CommentTemplate),
    className: 'comment-module module',

    events: {
      'click': 'toggleComments'
    },

    initialize: function () {
      var self = this;
      self.commentsOpen = false;
    },

    render: function () {
      var self = this;

      self.$el.html(this.template());
      return this;
    },

    remove: function () {

    },

    toggleComments: function () {
      var self = this;
      if (self.commentsOpen) {
        self.closeComments();
      } else if (!self.commentsOpen) {
        self.openComments();
      }
    },

    openComments: function () {
      var self = this;

      app.trigger('comments.open');
      app.trigger('stream.shrink');

      self.$el.addClass('open');
      self.commentsOpen = true;
    },

    closeComments: function () {
      var self = this;
      app.trigger('comments.close');
      app.trigger('stream.grow');

      self.$el.removeClass('open');
      self.commentsOpen = false;
    }

  });
});
