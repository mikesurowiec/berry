/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/bar_modules/photo-bar.ejs',
  'vendor/jquery.ui.widget',
  'vendor/jquery.fileupload'
],

function($, _, Backbone, app, PhotoTemplate) {
  'use strict';

  app.View.BarModule.Photo = Backbone.View.extend({
    template: _.template(PhotoTemplate),
    className: 'photo-module module',

    events: {
      'click': 'togglePhotoDialog',
      'click .cancel': 'cancel',
      'click .save': 'save',
      'change input[type="file"]': 'photosChanged'
    },

    initialize: function () {
      var self = this;
    },

    render: function () {
      var self = this;

      self.$el.html(this.template());

      return this;
    },

    remove: function () {

    },

    togglePhotoDialog: function (evt) {
      if (evt.target.nodeName === "I" || evt.target === this.$el.get(0)) {
        if (!this.open) {
          this.openDialog();
        } else {
          this.closeDialog();
        }
      }
    },

    cancel: function () {
      this.$el.find('form input[type="file"]').val();
      this.closeDialog();
    },

    postRender: function () {
      var self = this;
      var $form = $('.direct-upload');
      var $progressBarWrap = $('.progress-bar-wrap');
      var $progressBar = $('.progress-bar');

      $form.fileupload({
        url: 'https://advntrs-dev.s3.amazonaws.com/',
        type: 'POST',
        autoUpload: true,
        replaceFileInput: false,
        dataType: 'xml',
        add: function (event, data) {
          $.get('/api/user/s3policy', function (policy) {
            policy = JSON.parse(policy);
            $form.find('input[name=key]').val(policy.clearKey);
            $form.find('input[name=policy]').val(policy.s3PolicyBase64);
            $form.find('input[name=signature]').val(policy.s3Signature);
            data.submit();
          });
        },
        send: function(e, data) {
          $progressBarWrap.addClass('show');
        },
        progress: function(e, data){
          var percent = Math.round((e.loaded / e.total) * 100)
          $progressBar.css('width', percent + '%')
        },
        fail: function(e, data) {
          console.log('failed to upload picture');
        },
        success: function(data) {
          // Here we get the file url on s3 in an xml doc
          var url = $(data).find('Location').text();
          var image = {
            original: url
          };
          var item = new app.Model.Item({
            type: 'photo',
            image: {
              original: url
            }
          });
          item.save(null, {
            success: function (model, response, options) {
              app.items.add(model);
              self.closeDialog();
            },
            error: function (model, response, options) {

            }
          });
        },
        done: function (event, data) {
          console.log('done!');
          $progressBarWrap.removeClass('show');
          $progressBar.css('width', 5);
        }
      });
    },

    save: function () {
      var self = this;
      // var input = this.$el.find('input[type="file"]')[0];
      // var file = input.files[0];

      // this.closeDialog();
    },

    progressHandling: function (e){
      if(e.lengthComputable){
        console.log(e.loaded);
        // $('progress').attr({value:e.loaded,max:e.total});
      }
    },

    photosChanged: function () {
      var input = this.$el.find('input[type="file"]')[0];
      var files = input.files;
      var $photoPreview = this.$el.find('.photo-preview');
      for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
          continue;
        }
        var reader = new FileReader();
        // Closure to capture the file information.
        reader.onload = (function(theFile) {
          return function(e) {
            // Render thumbnail.
            var span = document.createElement('span');
            span.innerHTML = ['<img class="thumb" src="', e.target.result,
                              '" title="', escape(theFile.name), '"/>'].join('');
            $photoPreview.append(span);
          };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    },

    closeDialog: function () {
      this.$el.removeClass('open');
      $('#stream').removeClass('obscure');
      this.open = false;
    },

    openDialog: function () {
      this.$el.addClass('open');
      $('#stream').addClass('obscure');
      this.open = true;
    },

    selectPhotos: function () {
      var self = this;
    },

  });
});
