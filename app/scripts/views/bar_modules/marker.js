/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/bar_modules/marker-bar.ejs'
],

function($, _, Backbone, app, Template) {
  'use strict';

  app.View.BarModule.Marker = Backbone.View.extend({
    template: _.template(Template),
    className: 'marker-module module',

    events: {
      'click': 'showMarkers'
    },

    initialize: function () {
      var self = this;
    },

    render: function () {
      var self = this;

      self.$el.html(this.template());
      return this;
    },

    remove: function () {

    },

    showMarkers: function () {
      var self = this;
    },

  });
});