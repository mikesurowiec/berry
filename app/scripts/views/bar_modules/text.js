/*global
  define: false
*/

define([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'vendor/text!templates/bar_modules/text-bar.ejs'
],

function($, _, Backbone, app, Template) {
  'use strict';

  app.View.BarModule.Text = Backbone.View.extend({
    template: _.template(Template),
    className: 'text-module module',

    events: {
      'click': 'toggleTextDialog',
      'click .cancel': 'cancel',
      'click .save': 'save'
    },

    initialize: function () {
      var self = this;
      self.open = false;
    },

    render: function () {
      var self = this;

      self.$el.html(this.template());
      return this;
    },

    remove: function () {

    },

    toggleTextDialog: function (evt) {
      if (evt.target.nodeName === "I"  || evt.target === this.$el.get(0)) {
        if (!this.open) {
          this.openDialog();
        } else {
          this.closeDialog();
        }
      }
    },

    closeDialog: function () {
      this.$el.removeClass('open');
      $('#stream').removeClass('obscure');
      this.open = false;
    },

    openDialog: function () {
      this.$el.addClass('open');
      $('#stream').addClass('obscure');
      this.open = true;
    },

    save: function (evt) {
      var self = this;
      var content = self.$el.find('textarea').val();
      var item = new app.Model.Item({type: 'text', content: content});
      item.save(null, {
        success: function (model, response, options) {
          app.items.add(model);
          self.$el.find('textarea').val('');
          self.closeDialog();
        },
        error: function (model, response, options) {

        }
      });
    },

    cancel: function (evt) {
      this.closeDialog();
      this.$el.find('textarea').val('');
    }

  });
});
